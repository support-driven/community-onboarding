# Triggers
 - Joins the #chit-chat channel
 - Uses `/foqal help` in default workspace
 - Says any of the following in the #career-development channel:
    - "channel info"

# Cards

## Welcome to Support Driven!
`Type: Dialog`

> Hi {{FIRST_NAME}},
>
> Thank you for being a part of our community!
> 
> As you’re exploring our community, it’s helpful to keep our Code of Conduct. 
> and our core values in mind -
> - Foster acceptance and belonging. Everyone can contribute if we can make space for them. There’s someone in the community that can learn from where you are in your journey.
> - Help others when you can. Helping others is your gift to give and it can come in many forms - responding to questions, asking questions, sharing your experience, connecting people, and more. 
> - Nurture relationships. Relationships are the fabric of our community and will define your experience here.
> 
> Getting around
> 
> Here are a few places to start:
> 
> - #chit-chat - this channel is for anyone to talk about anything!
> - #good-news - share your good news with us!
>   - #vent - when things get ridiculous and you have to tell someone about it
> - #about - if you have a question about Support Driven itself, or want to let us know about a member not following policies, do so here.
>
> You can use /channel help to bring up this info in #chit-chat or any channel that has an info sheet. 
>
> Understanding how we name channels in Slack
> - Channels that support a specific service or app start with #u- like #u-zendesk, #u-helpscout, #u-freshworks, etc.
> - Channels for finding people that live in the same geographic area start with #zlocal- like #zlocal-san-francisco, #zlocal-uk, #zlocal-sydney, etc. 
> - Channels for finding people with similar hobbies and interests start with #x- like #x-fuzzyfriends, #x-parenting, #x-music, etc. 
> 
> To help us point you to useful community resources, which of these best describes you?


### Actions
| Title | Description | Target | 
| ----- | ----------- | ------ |
| Customer Support & Success | Do you have questions related to customer support and success? Do you want to connect with like-minded people who do what you do? | [Customer support/Success](#customer-supportsuccess) |
| Jobseeker | Are you looking for a job? | [Jobseeker](#jobseeker) |
| Vendors & Consultants | Are you a vendor that sells to people in Customer Support and Success? Join our #vendors channel | [Vendors & Consultants](#vendors--consultants) |


## Customer Support/Success
`Type: Dialog`

> Welcome!
> 
> If you’re looking for other folks that care about doing good work in Customer Support and Success, you’ve come to the right place. 
> 
> You’ll find like-minded people in - 
> - #customer-experience
> - #customer-success
> - #leadership
> - #knowledge-management
> - #operations
> - #trust-safety-security
> 
> Where do you support your users? These channels are great places to ask about best practices and share strategies and tips. 
> 
> - #support-chat
> - #support-email
> - #support-phone
> - #support-slack
> - #support-social-media
> 
> Browse our channels in Slack to see other places where like-minded people are gathering. 
> 
> If you’ve got questions about the community, you can ask them in #about or send a DM to @scott



## Jobseeker
`Type: Dialog`

> Whether you’re looking for your first job in customer support or success or your next one, we’ve got you covered.
> 
> 
> ##### #career-development
> Strategies and tips on resumes, interviews, negotiating, professional development, career moves, and more.
> 
> ##### #job-board
> Share the jobs you’re hiring for and the best jobs you’ve found. 


## Vendors & Consultants
`Type: Dialog`

> Vendors are an important part of our community and we’re glad to have you here. We’ve seen a lot of good things come out of having a neutral space where providers and customers can connect and 
> 
> All members are expected to follow our Code of Conduct and here a few things that are good to keep in mind as a vendor -
> 
> - **Context matters**. It’s the difference between someone welcoming your product or service because they asked for solutions and someone feeling that they’re being spammed because you injected your product or service into a conversation. 
> - **Relationships matter.** The more you contribute and engage with people in the community, the more they’re willing to respond when you ask. 
> - Do not direct message or contact members directly unless you have a relationship with them. 
> - Do not use our community to promote or advertise - including your content
> - You can share offers in our #offers channel. Requests to take surveys, give feedback, and similar requests can be made in our #requests channel.
> 
> In addition to #vendors, we also have channels for #consulting and #outsourcing. 
> 
> If you’re interested in supporting the community and becoming a Supporting Company, please get in touch with @scott.



