# Triggers
 - Joins the #career-development channel
 - Uses `/foqal career-development` in #team-channel-hosts
 - Says any of the following in the #career-development channel:
    - "channel info"

# Cards

## Welcome to Career Development
`Type: Dialog`

> Career Development is a channel helping you improve your career and find your next job. If you are here, you should also check out our #job-board.
>
> **Questions**? Ask one of our channel hosts:
> - Alison Groves
> - Harsha Chelle
> - Kimberly Hannam
>
> **Salary Survey:**
> We do this salary survey for the people in customer support. We do it to shed light onto conversations about pay. We believe customer 
> support is a fantastic career choice and we want to help people get paid what they’re worth. Link: [https://supportdriven.com/salary-survey/]()
>
> **Need some help?**
> Let us first ask what you are here to do.

### Actions
| Title | Description | Target | 
| ----- | ----------- | ------ |
| Job Seeker | I am a job seeker. | [Job Seeker](#Job-Seeker) |
| Hiring Manager | I am hiring someone as a manager or recruiter | [Hiring Manager](#Hiring-Manager) |





## Job Seeker
Type: Dialog

> As a Job Seeker, here are some resources that might be helpful to you:
> 
> - #job-board - Look at and apply to jobs here
> - https://jobs.supportdriven.com - Post your resume or apply to others.
> - https://supportdriven.com/salary-survey - This Salary survey is for the people in customer support to shed light 
onto conversations about pay. We believe customer support is a fantastic career choice and we want to help people get paid what they’re worth.




## Hiring Manager
Type: Dialog

> As a Hiring Manager, here are some resources that might be helpful to you:
>
> - #job-board - Post your job and see the job post of others.
> - jobs.supportdriven.com - Get your job in front of 5k+ qualified support professionals across the Support Driven community, newsletter, and social media channels



