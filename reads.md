# Triggers
 - Joins the #reads channel
 - Uses `/foqal reads` in default workspace
 - Says any of the following in the #u-soon channel:
    - "channel info"

# Cards

## Welcome to the reads channel
`Type: Dialog`
>
>Our goal for #reads is to create a space to share  items (books, blog posts, etc) you found helpful or interesting in some way. What this means is, for the most part, you should share things which you did not personally write.
>
>It’s a channel for reading recommendations, not content promotion. For example, it’s okay to share the occasional personal blog post you wrote, especially if the emphasis of that is on sharing your experience. But please do not share every post you write as that puts the emphasis on promotion rather than community sharing.
>
>We also ask that vendors, consultants, and other businesses not use the channel to promote their own material. You’re welcome to share those types of materials in #bulletin-board. If you’d like to partner on content you feel is particularly relevant to the community please ping @scott.
