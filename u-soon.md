# Triggers
 - Joins the #u-soon channel
 - Uses `/foqal u-soon` in default workspace
 - Says any of the following in the #u-soon channel:
    - "channel info"

# Cards

## Welcome to the Soon User Group channel
`Type: Dialog`

> Welcome to the official Soon :sd: channel
> 
> Soon is a collaborative scheduling software for customer support teams. We're here to help you learn more about the product, get to know other Soon users and we're eager to find out about you! 
> 
> **Quick question**  
> Please introduce yourself by sharing any of or all of the following so we can get to know you better:
> - Support operating hours
> - Team size
> - Channels used for support
> - Number of shifts
> - Fun fact about your company
> 
> **Questions? Ask one of our channel hosts:**  
> Alessandro Cardinali - @Alessandro Cardinali  
> Olaf Jacobson - @Olaf Jacobson
