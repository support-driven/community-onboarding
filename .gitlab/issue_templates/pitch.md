Inspired by -> [Shape Up, Write the Pitch](https://basecamp.com/shapeup/1.5-chapter-06)

## Pitch Title
Put the title or name of your pitch here

## Problem
Describe the problem, opportunity, or use case that you're thinking about.

Establishing the problem also lets us have a clearer conversation later when it’s time to pitch the idea or bet on it. We want to be able to separate out the discussion about the demand so we don’t spend time on a good solution that doesn’t address a valuable problem.

How far you have to go to spell out the problem will depend on how much context you share with the people reading the write-up. The best problem definition consists of a single specific story that shows why the status quo doesn’t work. This gives you a baseline to test fitness against. People will be able to weigh the solution against this specific problem—or other solutions if a debate ensues—and judge whether or not that story has a better outcome with the new solution swapped in.

## Appetite
How much time do we want to spend on this? Are there any other constraints to consider?

Stating the appetite in the pitch prevents unproductive conversations. There’s always a better solution. The question is, if we only care enough to spend two weeks on this now, how does this specific solution look?

Anybody can suggest expensive and complicated solutions. It takes work and design insight to get to a simple idea that fits in a small time box. Stating the appetite and embracing it as a constraint turns everyone into a partner in that process.

## Solution
Describe your solution here.

A problem without a solution is unshaped work. Giving it to a team means pushing research and exploration down to the wrong level, where the skillsets, time limit, and risk profile (thin vs. heavy tailed) are all misaligned.

If the solution isn’t there, someone should go back and do the shaping work on the shaping track. It’s only ready to bet on when problem, appetite, and solution come together. Then you can scrutinize the fit between problem and solution and judge whether it’s a good bet or not.

## No Gos
If there’s anything we’re not doing in this concept, it’s good to mention it here.
